# GPU-Accelerated Spring '83 Generation
Generate Ed25519 keys on your GPU for high performance!!

Currently Windows only, sorry.

## Getting Started
1. Set CUDA_DIR env variable to cuda directory. (e.g. C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.1)
2. Set BOOST_DIR env variable to boost directory. (e.g. C:\boost)
3. Build the boost library. (you have to run `boostrap` followed by the executable that gets generated)

## Notes
USE AT YOUR OWN RISK- I did not vet the random number generator that makes these keys.

## Reference
Great thanks to
    - https://github.com/iIIusi0n/ed25519-gpu
For doing all the hard work (all I had to do was tweak their code to properly generate private keys in the correct format for So83)

- https://www.boost.org/doc/libs/1_66_0/libs/compute/doc/html/index.html
- https://github.com/orlp/ed25519
- https://github.com/bkerler/opencl_brute
